#!/bin/sh

format=raw
verbose=

usage() {
    cat >&2 <<EOF
Usage: $0 [--verbose] [--format={raw|shell|graph}] [--] {file.dll|file.exe} ...
EOF
    exit 1
}

test $# != 0 || usage

while test $# != 0; do
    case "$1" in
    --format\=*)
	case "$(echo "$1" | cut -d= -f2)" in
	raw)   format=raw   ;;
	shell) format=shell ;;
	graph) format=graph ;;
	*) usage ;;
	esac
	;;
    --verbose)
	verbose=1
	;;
    --)
	shift
	break
	;;
    *)
	break
	;;
    esac

    shift
done

. mingw-environment.sh

OBJDUMP=${OBJDUMP:-$TARGET-objdump}
ALL_DLLS="$(find "$MINGW_PREFIX/bin" "$MINGW_PREFIX/lib" -name "*.dll")"

dump_dlls() {
    $OBJDUMP -x -- "$1" | grep 'DLL Name:' | cut -d: -f2
}

list_deps() {
    # $1 -> filename
    # $2 -> indent level
    # $3 -> print full filename

    if test "$2" -ge 0; then
	printf '%*s%s\n' $(expr "$2" \* 2) '' \
	    "$(test "$3" = 1 && echo "$1" || echo "$(basename -- "$1")")"
    fi

    for dep in $(dump_dlls "$1"); do
	installed="$(echo "$ALL_DLLS" | grep "$dep")"

	if test "$installed"; then
	    list_deps "$installed" $(expr "$2" + 1) $3
	elif test "$verbose"; then
	    echo "'$(basename -- "$1")' needs '$dep'," \
		"which is not installed." >&2
	fi
    done
}

shellscriptify() {
    cat <<EOF
#!/bin/sh
# This script was generated by $(basename $0).
# Edit the list below to suit your needs.

cp \\
$(sed -e 's/^/    /' -e 's/$/ \\/')
    .
EOF
}

# "raw" format defaults
initindent=-1
longnames=1

case "$format" in
shell)
    initindent=-1
    longnames=1
    ;;
graph)
    initindent=0
    longnames=0
esac

while test $# != 0; do
    list_deps "$1" $initindent $longnames
    shift
done | {
    case "$format" in
    raw)
	sed 's/^\s\+//' | sort | uniq
	;;
    shell)
	sed 's/^\s\+//' | sort | uniq | shellscriptify
	;;
    graph)
	cat
	;;
    esac
}
